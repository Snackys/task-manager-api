const mongoose = require('mongoose')

mongoose.connect('mongodb://snackys:azerty1@ds131384.mlab.com:31384/task-manager', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
})
